<?php

namespace Drupal\pagedesigner_parts\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeStorageInterface;
use Drupal\pagedesigner\Service\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a pagedesigner part block.
 *
 * This block can be used to display the content of a page anywhere
 * in the block layout.
 *
 * @Block(
 *   id = "pagedesigner_part",
 *   admin_label = @Translation("Pagedesigner part"),
 * )
 */
class PagedesignerPart extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The node to render.
   *
   * @var \Drupal\node\Entity\Node
   */
  protected $node = NULL;

  /**
   * The pagedesigner renderer service.
   *
   * @var \Drupal\pagedesigner\Service\Renderer
   */
  protected $renderer = NULL;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * Current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The storage handler class for nodes.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected NodeStorageInterface $nodeStorage;

  /**
   * {@inheritDoc}
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values
   *   keyed by configuration option name.
   *   The special key 'context' may be used to initialize the defined contexts
   *   by setting it to an array of context values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\pagedesigner\Service\Renderer $renderer
   *   The pagedesigner renderer service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The current route match.
   * @param \Drupal\Core\Language\LanguageManager $languageManager
   *   The language manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager service.
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      Renderer $renderer,
      CurrentRouteMatch $currentRouteMatch,
      LanguageManager $languageManager,
      AccountInterface $currentUser,
      EntityTypeManagerInterface $entity_type_manager
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->languageManager = $languageManager;
    $this->currentUser = $currentUser;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritDoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $container->get('pagedesigner.service.renderer'),
        $container->get('current_route_match'),
        $container->get('language_manager'),
        $container->get('current_user'),
        $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    // Create build array with defaults.
    $build = [];

    // Get block configuration.
    $config = $this->getConfiguration();
    $assignedNode = -1;

    // Check if assigned node id is avaiable.
    if (isset($config['assignedNode'])) {
      $assignedNode = $config['assignedNode'];
      $nodeId = $this->currentRouteMatch->getRawParameter('node');

      // Only render when not on assigned node.
      if ($nodeId != $assignedNode) {
        /** @var \Drupal\node\Entity\Node $node */
        $this->node = $this->nodeStorage->load($assignedNode);

        // Check if the node is available.
        if ($this->node != NULL) {

          $language = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
          if ($this->node->hasTranslation($language)) {
            $this->node = $this->node->getTranslation($language);
            $container = $this->node->field_pagedesigner_content->entity;
            if ($container != NULL && $container->hasTranslation($language)) {
              $container = $container->getTranslation($language);

              if (!$this->currentUser->isAnonymous()) {
                // Render node for logged in users.
                $this->renderer->render($container, $this->node);
              }
              else {
                // Render node for anonymous users (public revisions only).
                $this->renderer->renderForPublic($container, $this->node);
              }

              // Get markup and additional css.
              $build = $this->renderer->getOutput();
            }
          }
        }
      }
      else {
        $build['#cache'] = ['max-age' => 0];
      }
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $node = NULL;
    $assignedNode = -1;
    if (isset($this->configuration['assignedNode'])) {
      $assignedNode = $this->configuration['assignedNode'];
      $node = $this->nodeStorage->load($assignedNode);
    }
    $form['assignedNode'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Assigned part'),
      '#target_type' => 'node',
      '#selection_settings' => ['target_bundles' => ['pagedesigner_part']],
      '#tags' => TRUE,
      '#size' => 30,
      '#maxlength' => 1024,
      '#default_value' => $node,
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['assignedNode'] = $form_state->getValue('assignedNode')[0]["target_id"];
  }

}
